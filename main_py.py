import pygame
import random
from pygame import Color, Rect


def handle_key_presses(world):
    # TODO: COMPLETE OTHER KEYS
    # TODO: STOP DUDE RUNNING OFF SCREEN

    pressed_keys = pygame.key.get_pressed()
    if pressed_keys[pygame.K_RIGHT]:
        world['dude']['x'] += world['dude']['speed']
    return world


def target_hit():
    #TODO: define collisions

    return False

def move_target(world):
    #TODO: fill in

    return world

def update_rectangles(world):
    #TODO: fill in target rectangle

    rectangles = {
            'dude': Rect(world['dude']['x'], world['dude']['y'], world['dude']['size'], world['dude']['size'])
        } 

    return rectangles 

def draw_screen(world, surface, rectangles):
	#TODO: fill in target
	
    pygame.draw.rect(
        surface,
        Color(0, 0, 0),
        Rect(0, 0, 500, 500)
    )

    pygame.draw.rect(
        surface,
        world['dude']['colour'],
        rectangles['dude']
    )


def close_window_if_x_button_has_been_pressed():
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            return True
    return False


if __name__ == '__main__':
    pygame.init()

    screen_width = 500
    screen_height = 500

    screen = pygame.display.set_mode((screen_width, screen_height))
    surface = pygame.Surface((screen_width, screen_height))

    stopping: bool = False

    world = {
        'dude': {
            'x': 10,
            'y': 10,
            'colour': Color(255, 0, 127),  # Sorry for the inconsistent naming of colour but I am BRITISH GOD DAMNIT!
            'size': 20,
            'speed': 3
        }

        #TODO: fill in target
    }

    
    # Rect(x, y, x-size, y-size)
    rectangles = {
        'dude': Rect(world['dude']['x'], world['dude']['y'], world['dude']['size'], world['dude']['size']),
        
        #TODO: fill in target
    }

    while not stopping:

        stopping = close_window_if_x_button_has_been_pressed()

        world = handle_key_presses(world)
    
    #   TODO: define target_hit()
    #    if target_hit():
    #        world = move_target(world)

        rectangles = update_rectangles(world)

        draw_screen(world, surface, rectangles)

        screen.blit(surface, (0, 0))
        pygame.display.update()

# STEP 1: Finish handle_key_presses
# STEP 2: Stop the dude from going off the screen
# STEP 3: Create a target in world and have it appear in a random location
# STEP 4: When the dude overlaps the target, move the target
