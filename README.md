# Python Game

Python game code for class

## Getting started

Want to run this on your own PC? There's 3 steps:
1. Download Python 3
2. Download pip3 install
3. Run: `pip3 install pygame --user`

Then to run the file, run:
```
python3 main_py.py
```
Look here for help navigating your terminal/command line: https://www.davidbaumgold.com/tutorials/command-line/
